defmodule ButtsInSeats.PageController do
  use ButtsInSeats.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end

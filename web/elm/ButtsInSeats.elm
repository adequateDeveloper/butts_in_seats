module ButtsInSeats exposing (..)

import Html exposing (..)
import Html.Events exposing (onClick)
import Html.Attributes exposing (class)


-- MODEL


type alias Seat =
    { seatNo : Int
    , occupied : Bool
    }


type alias Model =
    List Seat


initialModel : Model
initialModel =
    [ { seatNo = 1, occupied = False }
    , { seatNo = 2, occupied = False }
    , { seatNo = 3, occupied = False }
    , { seatNo = 4, occupied = False }
    , { seatNo = 5, occupied = False }
    , { seatNo = 6, occupied = False }
    , { seatNo = 7, occupied = False }
    , { seatNo = 8, occupied = False }
    , { seatNo = 9, occupied = False }
    , { seatNo = 10, occupied = False }
    , { seatNo = 11, occupied = False }
    , { seatNo = 12, occupied = False }
    ]



-- UPDATE


type Msg
    = Toggle Seat


update : Msg -> Model -> Model
update msg model =
    case msg of
        -- return the new model with the selected seat' occupied property toggled
        Toggle selectedSeat ->
            let
                -- function updateSeat/1:
                -- if the next seat from the model has a seat # that matches
                -- the seat # of the currently selected seat then return a
                -- new seat with the same # and with the occupied value toggled.
                -- else return a new seat with the same values as the
                -- next seat in the model
                _ =
                    Debug.log "Selected seat before updating: " selectedSeat

                updateSeat nextSeat =
                    if nextSeat.seatNo == selectedSeat.seatNo then
                        { nextSeat | occupied = not nextSeat.occupied }
                    else
                        nextSeat
            in
                List.map updateSeat model



-- VIEW


view : Model -> Html Msg
view model =
    ul [ class "seats" ] (List.map seatItem model)


seatItem : Seat -> Html Msg
seatItem seat =
    let
        seatStatus =
            if seat.occupied then
                "occupied"
            else
                "available"
    in
        li
            [ class ("seat " ++ seatStatus)
            , onClick (Toggle seat)
            ]
            [ text (toString seat.seatNo) ]



-- ENTRY POINT


main : Program Never Model Msg
main =
    Html.beginnerProgram
        { model = initialModel
        , view = view
        , update = update
        }

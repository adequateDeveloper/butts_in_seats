# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :butts_in_seats,
  ecto_repos: [ButtsInSeats.Repo]

# Configures the endpoint
config :butts_in_seats, ButtsInSeats.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "LYFYgzx0PF3pSiP6J7kVav2pZ2SmmJFwEDTu2e8/QGi9+240lnRuItV48pHi/JXW",
  render_errors: [view: ButtsInSeats.ErrorView, accepts: ~w(html json)],
  pubsub: [name: ButtsInSeats.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
